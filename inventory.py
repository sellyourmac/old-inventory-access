import cookielib
import urllib
import urllib2
import pickle
import socket
import sys

try:
    from brotherprint import BrotherPrint as BP
except ImportError:
    print "You may need to install brotherprint >=0.1.1"
    #sys.exit(1)

item_url = 'http://inv.sellyourmac.com/inventory/index.php?action=itemView&item='
main_url = 'http://inv.sellyourmac.com/inventory/index.php'
dupe_url = 'http://inv.sellyourmac.com/inventory/index.php?action=formSubmit'
curr_output_file = "/Users/macuser/Desktop/output.csv"
master_output_file = "/Users/macuser/Desktop/MASTEROUTPUT.csv"
try:
    pw_file = open('./pws','rb')
except IOError,e:
    pw_file = open('./pws','w+b')
    pickle.dump({'wtekulve':'XavaCdAw'},pw_file)
    pw_file.close()
    pw_file = open('./pws','rb')
pws=pickle.load(pw_file)
pw_file.close()
'''values = {'User' : 'wtekulve',
          'Pass' : 'XavaCdAw',
          'url'  : '/index.php',
          'login': 'Log in'     }

data = urllib.urlencode(values)
cookies = cookielib.CookieJar()

opener = urllib2.build_opener(
    urllib2.HTTPRedirectHandler(),
    urllib2.HTTPHandler(debuglevel=0),
    urllib2.HTTPSHandler(debuglevel=0),
    urllib2.HTTPCookieProcessor(cookies))

print the_page'''
class InventorySite:
    def __init__(self,user=None,passw=None):
        self.user = user
        self.passw = passw
        self.logged_in = False

    def _post(self,url,vals):
        new_data = urllib.urlencode(vals)
        req = self.opener.open(url,new_data)
        resp = req.read()
        return resp

    def login(self):
        if self.user and self.passw:
            pass
        elif len(pws) > 0:
            #print "Saved users:"
            #for k,v in pws.items():
            #    print "    %s" % k
            self.user = self.user if self.user is not None else raw_input("Enter username: ")
            if self.user in pws:
                self.passw = pws[self.user]
            else:
                self.passw = raw_input("Enter password: ")
                pws[self.user] = self.passw
                pws_file = open("./pws", 'r+b')
                pickle.dump(pws, pws_file)
                pws_file.close()
        else:
            self.user = raw_input("Enter username: ")
            self.passw = raw_input("Enter password: ")
            pws[self.user] = self.passw
            pws_file = open("./pws", 'r+b')
            pickle.dump(pws, pws_file)
        values = {'User' : self.user,
                  'Pass' : self.passw,
                  'url'  : '/index.php',
                  'login': 'Log in'     }

        self.data = urllib.urlencode(values)
        self.cookies = cookielib.CookieJar()

        self.opener = urllib2.build_opener(
            urllib2.HTTPRedirectHandler(),
            urllib2.HTTPHandler(debuglevel=0),
            urllib2.HTTPSHandler(debuglevel=0),
            urllib2.HTTPCookieProcessor(self.cookies))
        response = self.opener.open(main_url, self.data)
        self.logged_in = True

    def get_pagebyitemid(self,id):
        new_url = item_url+id
        return self.opener.open(new_url).read()

    def get_sender(self, html):
        return self.get_item(html,'desc_SenderName')

    def get_senderemail(self, html):
        return self.get_item(html,'desc_SenderEmail')

    def get_ipo(self,html):
        return self.get_item(html,'desc_SenderIPO')

    def get_carrier(self,html):
        carrier = self.get_attr(html,'product_Service')
        if "at&t" in carrier.lower():
            ret = "ATT"
        elif "verizon" in carrier.lower():
            ret = "Verizon"
        elif "sprint" in carrier.lower():
            ret = "Sprint"
        elif  "t" in carrier.lower() and "mobile" in carrier.lower():
            ret = "T-mobile"
        elif "unlock" in carrier.lower():
            ret = "Unlocked"
        else:
            ret = "None"
        return ret

    def get_capacity(self,html):
        cap = self.get_attr(html,'product_HDSize')
        ret = 0
        try:
            ret = int(cap.split()[0])
        except ValueError:
            pass
        except IndexError:
            pass
        return ret

    def get_color(self,html):
        color = self.get_attr(html,'product_Color')
        if "+" in color:
            ret = color.replace("+"," ")
        else:
            ret = color
        return ret.title()

    def get_cosmetic(self,html):
        start = html.find('desc_CosVal')
        start = html.find('<option value=\'nochange\'>',start)
        start+=25
        end = html.find('<',start)
        return html[start:end]

    def get_info(self,html):
        start = html.find('desc_OInfo')
        start = html.find('>',start)
        start+=2
        end = html.find("<",start)
        return html[start:end]

    def get_itemid(self,html):
        start = html.find('window.location')
        start = html.find("LA",start)
        end = html.find("'",start)
        return html[start:end]

    def get_item(self,html,name):
        start = html.find(name)
        start = html.find("value='",start)
        start+=7
        end = html.find("'",start)
        return html[start:end]

    def _get_flag(self,html,name):
        start = html.find(name)
        end = html.find('>',start)
        if 'checked'.lower() in html[start:end].lower():
            return True
        return False

    def get_attr(self,html,name):
        start = html.find(name)
        start = html.find("value='nochange'>",start)
        start+=17
        end = html.find("<",start)
        return html[start:end]

    def get_notes(self,html):
        start = html.find('desc_OInfo')
        start = html.find('>',start)
        start += 1
        end = html.find('</textarea>',start)
        return html[start:end]

    def get_speed(self,html):
        return self.get_attr(html,'product_HDSpeed')

    def get_charger_flag(self,html):
        return self._get_flag(html,'flags_ChargerIncluded')

    def get_box_flag(self,html):
        return self._get_flag(html,'flags_BoxIncluded')

    def get_auto_email_flag(self,html):
        return self._get_flag(html,'flags_AutoEmailsOff')

    def get_paid_flag(self,html):
        return self._get_flag(html,'flag_IsPaid')

    def get_flags(self,page):
        ret = {}
        charger = self.get_charger_flag(page)
        box = self.get_box_flag(page)
        auto_email = self.get_auto_email_flag(page)
        paid = self.get_paid_flag(page)
        if charger:
            ret['flags_ChargerIncluded'] = 'on'
        if box:
            ret['flags_BoxIncluded'] = 'on'
        if auto_email:
            ret['flags_AutoEmailsOff'] = 'on'
        if paid:
            ret['flags_IsPaid'] = 'on'

        return ret

    def set_model_number(self,itemid,modelno,modelyear=None):
        page = self.get_pagebyitemid(itemid).replace("\r","")
        flags = self.get_flags(page)
        vals = { 'itemID':itemid,
                 'product_ModelNumber':modelno,
                 'formName':'modifyItem',
                 'UpdateItem':'Update Item',
                 'products_Status':'nochange' }
        if modelyear:
            vals['desc_Year'] = modelyear
        vals = dict(vals.items() + flags.items())
        self._post(dupe_url,vals)

    def prepend_oInfo(self,itemid,text):
        page = self.get_pagebyitemid(itemid).replace("\r","")
        curr_notes = self.get_notes(page).replace("\r","")
        new_notes = text+"\n\n"+curr_notes

        #box, charger, auto emails
        flags = self.get_flags(page)

        vals = { 'itemID':itemid,
                 'desc_OInfo':new_notes,
                 'formName':'modifyItem',
                 'UpdateItem':'Update Item',
                 'products_Status':'nochange' }
        vals = dict(vals.items() + flags.items())
        self._post(dupe_url,vals)

    def duplicate(self):
        master = raw_input("Enter master number: ")
        count = int(raw_input("Enter number of duplicate entries: "))
        if count > 10:
            yes = raw_input("Count is over ten, enter 1234 to continue: ")
            if yes != '1234':
                return
        if count > 20:
            print "Fuck you too I'm doing 20 instead."
            count = 20
        multiples = int(raw_input("Enter number of tags per computer: "))
        curr_url = item_url + master
        curr_master = master
        curr_output = open(curr_output_file,'wb')
        curr_output.write("Number\n")
        master_output = open(master_output_file,'r+b')
        if "Number" not in master_output.readline():
            master_output.write("Number\n")
        else:
            master_output.seek(0,2)
        list = []
        for i in xrange(count):
            response = self.opener.open(curr_url, self.data)
            the_page = response.read()
            sender = self.get_sender(the_page)
            sender_email = self.get_senderemail(the_page)
            ipo = self.get_ipo(the_page)
            info = self.get_info(the_page)
            vals = {'formName':'modifyItem',
                    'itemID':curr_master,
                    'products_Status':'nochange',
                    'product_Model':'nochange',
                    'DuplicateItem':'Duplicate Item',
                    'desc_ReturnAction':'nochange',
                    'desc_ShipsWith':'nochange',
                    'desc_PreferredPayment':'nochange',
                    'desc_SenderEmail':sender_email,
                    'desc_SenderName':sender,
                    'desc_SenderIPO':ipo,
                    'desc_OInfo':info
                    }
            server_resp = self._post(dupe_url,vals)
            with open('./output.html', 'w') as of:
                of.write(server_resp)
            newid=self.get_itemid(server_resp)
            print newid
            curr_output.write(newid+"\n")
            master_output.write(newid+"\n")
            curr_master=newid
            curr_url = curr_url[:-8]+curr_master
            list.append(newid)
        bro_print(list,multiples)

def bro_print(list,number):
    f_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    f_sock.connect(('192.168.1.3',9100))
    pj = BP(f_sock)
    pj.template_mode()
    pj.template_init()
    pj.choose_template(1)

    for item in list:
        for i in xrange(number):
            pj.select_and_insert('barcode',item)
            pj.select_and_insert('number',item)
            pj.template_print()
    f_sock.close()

def pass_fix():
    if len(pws) == 0:
        print "No user info stored!"
        return

    for k,v in pws.items():
        print "    %s" % k

    name = raw_input("Enter username to modify: ")

    if name not in pws:
        print "No such username!"
        return

    print "Options:"
    print "    1) change password"
    print "    2) delete user"

    opt = raw_input("Enter choice: ")

    if opt=='1':
        password = raw_input("Enter new password: ")
        pws[name] = password
    elif opt=='2':
        del pws[name]
    else: return
    pws_file = open("./pws", 'r+b')
    pickle.dump(pws, pws_file)

    pws_file.close()

def main():
    print "Options:"
    print "    1) Duplicate from master"
    print "    2) Fix stored password"
    x = raw_input("Enter choice: ")
    if x=='1':
        z = InventorySite()
        z.login()
        z.duplicate()
    if x=='2':
        pass_fix()

if __name__ == '__main__':
    main()
